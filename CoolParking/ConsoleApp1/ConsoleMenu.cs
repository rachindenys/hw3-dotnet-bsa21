﻿using ConsoleApp1.Models;
using ConsoleApp1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public static class ConsoleMenu
    {
        private static ParkingHttpService _parkingService = new ParkingHttpService();
        private static VehiclesHttpService _vehiclesService = new VehiclesHttpService();
        private static TransactionsHttpService _transactionsService = new TransactionsHttpService();

        private static List<Option> options = new List<Option>
            {
                new Option("Show parking balance", () => ShowParkingBalance()),
                new Option("Show income for the current period", () =>  ShowCurrentIncome()),
                new Option("Show parking capacity", () =>  ShowParkingCapacity()),
                new Option("Show transactions for the current period", () =>  ShowCurrentTransactions()),
                new Option("Show transactions history", () =>  ShowTransactionsHistory()),
                new Option("Show vehicles, currently placed on parking", () =>  ShowVehicles()),
                new Option("Place vehicle on parking", () =>  PlaceVehicle()),
                new Option("Remove vehicle from parking", () =>  RemoveVehicle()),
                new Option("Top up vehicle balance", () =>  TopUpVehicle()),
                new Option("Exit", () => Environment.Exit(0)),
            };



        public static void Start()
        {
            try
            {
                int index = 0;
                ConsoleKeyInfo keyinfo;
                do
                {
                    WriteMenu(options, options[index]);
                    keyinfo = Console.ReadKey();

                    // Handle each key input (down arrow will write the menu again with a different selected item)
                    if (keyinfo.Key == ConsoleKey.DownArrow)
                    {
                        if (index + 1 < options.Count)
                        {
                            index++;
                            WriteMenu(options, options[index]);
                        }
                    }
                    if (keyinfo.Key == ConsoleKey.UpArrow)
                    {
                        if (index - 1 >= 0)
                        {
                            index--;
                            WriteMenu(options, options[index]);
                        }
                    }
                    // Handle different action for the option
                    if (keyinfo.Key == ConsoleKey.Enter)
                    {
                        options[index].Selected.Invoke();
                        index = 0;
                    }
                }
                while (keyinfo.Key != ConsoleKey.X);
            }
            catch (Exception e)
            {
                Console.WriteLine($"You can't perfom this operation: {e.Message}");
                Console.ReadKey();
                return;
            }

            Console.ReadKey();
        }
        public static void ShowParkingBalance()
        {
            Console.Clear();
            Console.WriteLine($"Current parking balance: {_parkingService.GetBalance().GetAwaiter().GetResult()}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void ShowCurrentIncome()
        {
            Console.Clear();
            Console.WriteLine($"Current parking income: {_parkingService.GetCurrentIncome().GetAwaiter().GetResult()}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void ShowParkingCapacity()
        {
            Console.Clear();
            var capacity =  _parkingService.GetCapacity().GetAwaiter().GetResult();
            var freePlaces =  _parkingService.GetFreePlaces().GetAwaiter().GetResult();
            Console.WriteLine($"Free places on parking: {freePlaces}\n" +
                $"Occupied places on parking: {capacity - freePlaces}\n" +
                $"Total places on parking: {capacity}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        static void WriteMenu(List<Option> options, Option selectedOption)
        {
            Console.Clear();

            foreach (Option option in options)
            {
                if (option == selectedOption)
                {
                    Console.Write("> ");
                }
                else
                {
                    Console.Write(" ");
                }

                Console.WriteLine(option.Name);
            }
        }

        public static void ShowCurrentTransactions()
        {
            Console.Clear();
            var transactions = _transactionsService.GetLastTransactions().GetAwaiter().GetResult();
            foreach (var tr in transactions)
            {
                Console.WriteLine($"{tr.transactionDate}: {tr.Sum} money withdrawn from vehicle with Id = {tr.VehicleId}.");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }
        public static void ShowTransactionsHistory()
        {
            Console.Clear();
                string transactions = _transactionsService.GetAllTransactions().GetAwaiter().GetResult();
                var transactionsList = transactions.Replace("\\r", "").Trim('"').Split("\\n", StringSplitOptions.RemoveEmptyEntries).ToList();
                transactionsList.ForEach(tr => Console.WriteLine(tr));
                Console.WriteLine("\n\nPress any key to go back...");
                Console.ReadKey();
        }

        public static void PlaceVehicle()
        {
                Console.Write("\n\n\nWrite new vehicle ID in format \"XX-YYYY-XX\", where X - letter, Y - number: ");
                var vehicleId = Console.ReadLine();
                Console.Write("\n\n\nWrite new vehicle Type (1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle): ");
                if (!Int32.TryParse(Console.ReadLine(), out int vehicleType) || (vehicleType>4 || vehicleType<1))
                {
                    throw new ArgumentException("Bad input!");
                }
                Console.Write("Write start vehicle balance: ");
                if (!decimal.TryParse(Console.ReadLine(), out decimal balance))
                {
                    throw new ArgumentException("Bad input!");
                }
                var response = _vehiclesService.AddVehicle(new Vehicle(vehicleId, (VehicleType)vehicleType, balance)).GetAwaiter().GetResult();
                if ((int)response.StatusCode == 400)
                {
                    throw new Exception("Request body is invalid!");
                }
                if ((int)response.StatusCode == 403)
                {
                    throw new Exception("Vehicle with same ID currently placed on parking!");
                }
           
        }
        public static void ShowVehicles()
        {
            Console.Clear();
            var vehicles = _vehiclesService.GetVehicles().GetAwaiter().GetResult();
            var counter = 1;
            Console.WriteLine($"seqno \t|\tID\t\t|\tBALANCE\t|\tTYPE");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                counter += 1;
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void RemoveVehicle()
        {
                Console.Clear();
                var vehicles = _vehiclesService.GetVehicles().GetAwaiter().GetResult();
                var counter = 1;
                Console.WriteLine($"seqno \t|\tID\t\t|\tBALANCE\t|\tTYPE");
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                    counter += 1;
                }
                Console.Write("\n\n\nWrite vehicle id to remove: ");
                var vehicleIndex = Console.ReadLine();
                var response = _vehiclesService.DeleteVehicle(vehicleIndex).GetAwaiter().GetResult();
                if ((int)response.StatusCode == 400)
                {
                    throw new Exception("Vehicle ID is invalid!");
                }
                if ((int)response.StatusCode == 404)
                {
                    throw new Exception("Vehicle not found!");
                }
                Console.WriteLine("Vehicle removed successfully!");
                Console.WriteLine("\n\nPress any key to go back...");
                Console.ReadKey();
        }

        public static void TopUpVehicle()
        {
                Console.Clear();
                var vehicles = _vehiclesService.GetVehicles().GetAwaiter().GetResult();
                var counter = 1;
                Console.WriteLine($"seqno \t|\tID\t\t|\tBALANCE\t|\tTYPE");
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                    counter += 1;
                }
                Console.Write("\n\n\nWrite vehicle id to topUp: ");
                var vehicleIndex = Console.ReadLine();
                Console.Write("Write top up amount: ");
                decimal.TryParse(Console.ReadLine(), out decimal sum);
                var response = _transactionsService.TopUpVehicle(vehicleIndex, sum).GetAwaiter().GetResult();
                if ((int)response.StatusCode == 400)
                {
                    throw new Exception("Request body is invalid!");
                }
                if ((int)response.StatusCode == 404)
                {
                    throw new Exception("Vehicle not found!");
                }
                Console.WriteLine("Topped up successfully!");
                Console.WriteLine("\n\nPress any key to go back...");
                Console.ReadKey();
        }

        protected class Option
        {
            public string Name { get; }
            public Action Selected { get; }

            public Option(string name, Action selected)
            {
                Name = name;
                Selected = selected;
            }
        }
    }
}
