﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    class LastTransactions
    {
        public decimal Sum { get; set; }
        public DateTime transactionDate { get; set; }
        public string VehicleId { get; set; }
    }
}
