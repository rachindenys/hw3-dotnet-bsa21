﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public enum VehicleType
    {
        PassengerCar = 1,
        Truck = 2,
        Bus = 3,
        Motorcycle = 4
    }
}
