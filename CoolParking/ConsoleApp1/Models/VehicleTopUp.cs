﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    class VehicleTopUp
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }
        public VehicleTopUp(string id, decimal balance)
        {
            Balance = balance;
            Id = id;
        }
    }
}
