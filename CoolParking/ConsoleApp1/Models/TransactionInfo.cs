﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    class TransactionInfo
    {
        [JsonProperty("sum")]
        public decimal Sum { get; }
        [JsonProperty("transactionDate")]
        public DateTime DateTime { get; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; }
    }
}
