﻿using ConsoleApp1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Services
{
    class TransactionsHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public TransactionsHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/");
        }
        public async Task<List<LastTransactions>> GetLastTransactions()
        {
            var content = await _httpClient.GetStringAsync("transactions/last");
            return JsonConvert.DeserializeObject<List<LastTransactions>>(content);
        }

        internal async Task<string> GetAllTransactions()
        {
            var content = await _httpClient.GetStringAsync("transactions/all");
            return content;
        }

        internal async Task<HttpResponseMessage> TopUpVehicle(string vehicleId, decimal Sum)
        {
            var content = JsonConvert.SerializeObject(new VehicleTopUp(vehicleId, Sum));
            var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            return await _httpClient.PutAsync("transactions/topUpVehicle", httpContent);
        }
    }
}
