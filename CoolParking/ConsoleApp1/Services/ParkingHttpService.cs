﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Services
{
    class ParkingHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public ParkingHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/parking/");
        }
        public async Task<decimal> GetBalance()
        {
            var content = await _httpClient.GetStringAsync("balance");
            return JsonConvert.DeserializeObject<decimal>(content);
        }

        public async Task<decimal> GetCurrentIncome()
        {
            var content = await _httpClient.GetStringAsync("income");
            return JsonConvert.DeserializeObject<decimal>(content);
        }

        internal async Task<int> GetFreePlaces()
        {
            var content = await _httpClient.GetStringAsync("freePlaces");
            return JsonConvert.DeserializeObject<int>(content);
        }

        internal async Task<int> GetCapacity()
        {
            var content = await _httpClient.GetStringAsync("capacity");
            return JsonConvert.DeserializeObject<int>(content);
        }
    }
}
