﻿using ConsoleApp1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Services
{
    class VehiclesHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();
        public VehiclesHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/vehicles/");
        }
        public async Task<List<Vehicle>> GetVehicles()
        {
            var content = await _httpClient.GetStringAsync("");
            var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(content);
            return vehicles;
        }

        public async Task<Vehicle> GetVehicleById(string id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            var vehicle = JsonConvert.DeserializeObject<Vehicle>(content);
            return vehicle;
        }

        public async Task<HttpResponseMessage> AddVehicle(Vehicle vehicle)
        {
            var content = JsonConvert.SerializeObject(vehicle);
            var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            return await _httpClient.PostAsync($"", httpContent);
        }

        public async Task<HttpResponseMessage> DeleteVehicle(string vehicleId)
        {
            var content = await _httpClient.DeleteAsync($"{vehicleId}");
            return content;
        }
    }
}
