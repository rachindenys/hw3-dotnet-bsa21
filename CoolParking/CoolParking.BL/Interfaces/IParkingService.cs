﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface IParkingService : IDisposable
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        ReadOnlyCollection<Vehicle> GetVehicles();
        Vehicle GetVehicles(string id);
        void AddVehicle(Vehicle vehicle);
        void RemoveVehicle(string vehicleId);
        void TopUpVehicle(string vehicleId, decimal sum);
        TransactionInfo[] GetLastParkingTransactions();
        decimal GetCurrentIncome();
        string ReadFromLog();
    }
}
