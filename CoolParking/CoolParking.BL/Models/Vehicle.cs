﻿using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get;}
        [JsonProperty("vehicletype")]
        public VehicleType VehicleType { get; }
        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (id == null)
            {
                throw new ArgumentException("Bad vehicle ID!");
            }
            var r = new Regex($"[A-z][A-z]-[0-9][0-9][0-9][0-9]-[A-z][A-z]");
            if (r.IsMatch(id))
            {
                Id = id.ToUpper();
            }
            else
            {
                throw new ArgumentException("Bad ID format!");
            }
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string id = "";
            Random rnd = new Random();
            for (int i=0; i < 4; i++)
            {
                id += (char)rnd.Next('a', 'z');
                if (id.Length == 2)
                {
                    id += '-';
                    for(int j=0; j < 4; j++)
                    {
                        id += (int)rnd.Next(0, 9);
                    }
                    id += '-';
                }
            }
            return id;
        }
    }
}