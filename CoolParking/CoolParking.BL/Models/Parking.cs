﻿using CoolParking.BL.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking parking;
        public List<Vehicle> Vehicles { get; set; } 
        public decimal Balance { get; set; }  
        public int Capacity { get; set; }
        public decimal CurrentIncome { get; set; }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Capacity = Settings.ParkingCapacity;
            Balance = Settings.StartParkingBalance;
        }

        public static Parking getParking()
        {
            if (parking == null)
                parking = new Parking();
            return parking;
        }

        public void CleanParking()
        {
            Vehicles = new List<Vehicle>();
        }

    }
}