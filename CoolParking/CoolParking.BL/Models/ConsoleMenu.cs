﻿using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public static class ConsoleMenu
    {
        private static ParkingService _parkingService = new ParkingService(
            new TimerService(), 
            new TimerService(), 
            new LogService(Settings.LogFilePath));
        
        private static List<Option> options = new List<Option>
            {
                new Option("Show parking balance", () => ShowParkingBalance()),
                new Option("Show income for the current period", () =>  ShowCurrentIncome()),
                new Option("Show parking capacity", () =>  ShowParkingCapacity()),
                new Option("Show transactions for the current period", () =>  ShowCurrentTransactions()),
                new Option("Show transactions history", () =>  ShowTransactionsHistory()),
                new Option("Show vehicles, currently placed on parking", () =>  ShowVehicles()),
                new Option("Place vehicle on parking", () =>  PlaceVehicle()),
                new Option("Remove vehicle from parking", () =>  RemoveVehicle()),
                new Option("Top up vehicle balance", () =>  TopUpVehicle()),
                new Option("Exit", () => Environment.Exit(0)),
            };



        public static void Start()
        {
            int index = 0;
            ConsoleKeyInfo keyinfo;
            do
            {
                WriteMenu(options, options[index]);
                keyinfo = Console.ReadKey();

                // Handle each key input (down arrow will write the menu again with a different selected item)
                if (keyinfo.Key == ConsoleKey.DownArrow)
                {
                    if (index + 1 < options.Count)
                    {
                        index++;
                        WriteMenu(options, options[index]);
                    }
                }
                if (keyinfo.Key == ConsoleKey.UpArrow)
                {
                    if (index - 1 >= 0)
                    {
                        index--;
                        WriteMenu(options, options[index]);
                    }
                }
                // Handle different action for the option
                if (keyinfo.Key == ConsoleKey.Enter)
                {
                    options[index].Selected.Invoke();
                    index = 0;
                }
            }
            while (keyinfo.Key != ConsoleKey.X);

            Console.ReadKey();
        }
        public static void ShowParkingBalance()
        {
            Console.Clear();
            Console.WriteLine($"Current parking balance: {_parkingService.GetBalance()}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void ShowCurrentIncome()
        {
            Console.Clear();
            Console.WriteLine($"Current parking income: {_parkingService.GetCurrentIncome()}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void ShowParkingCapacity()
        {
            Console.Clear();
            Console.WriteLine($"Free places on parking: {_parkingService.GetFreePlaces()}\n" +
                $"Occupied places on parking: {_parkingService.GetCapacity() - _parkingService.GetFreePlaces()}\n" +
                $"Total places on parking: {_parkingService.GetCapacity()}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        static void WriteMenu(List<Option> options, Option selectedOption)
        {
            Console.Clear();

            foreach (Option option in options)
            {
                if (option == selectedOption)
                {
                    Console.Write("> ");
                }
                else
                {
                    Console.Write(" ");
                }

                Console.WriteLine(option.Name);
            }
        }

        public static void ShowCurrentTransactions()
        {
            Console.Clear();
            var transactions = _parkingService.GetLastParkingTransactions();
            foreach (var tr in transactions)
            {
                Console.WriteLine(tr.ActionMessage);
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }
        public static void ShowTransactionsHistory()
        {
            Console.Clear();
            var transactions = _parkingService.ReadFromLog();
            Console.WriteLine(transactions);
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void PlaceVehicle()
        {
            Console.Write("\n\n\nWrite new vehicle ID in format \"XX-YYYY-XX\", where X - letter, Y - number: ");
            var vehicleId = Console.ReadLine();
            Console.Write("\n\n\nWrite new vehicle Type (1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle): ");
            var vehicleType = Int32.Parse(Console.ReadLine());
            Console.Write("Write start vehicle balance: ");
            decimal balance = decimal.Parse(Console.ReadLine());
            _parkingService.AddVehicle(new Vehicle(vehicleId, (VehicleType)vehicleType, balance));
        }
        public static void ShowVehicles()
        {
            Console.Clear();
            var vehicles = _parkingService.GetVehicles();
            var counter = 1;
            Console.WriteLine($"seqno \t|\tID\t\t|\tBALANCE\t|\tTYPE");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                counter += 1;
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void RemoveVehicle()
        {
            Console.Clear();
            var vehicles = _parkingService.GetVehicles();
            var counter = 1;
            Console.WriteLine($"seqno \t|\tID\t\t|\tBALANCE\t|\tTYPE");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                counter += 1;
            }
            Console.Write("\n\n\nWrite vehicle sequence number: ");
            var vehicleIndex = Int32.Parse(Console.ReadLine());
            var vehicleId = _parkingService.GetVehicles()[vehicleIndex - 1].Id;
            _parkingService.RemoveVehicle(vehicleId);
            Console.WriteLine("Vehicle removed successfully!");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void TopUpVehicle()
        {
            Console.Clear();
            var vehicles = _parkingService.GetVehicles();
            var counter = 1;
            Console.WriteLine($"seqno \t|\tID\t\t|\tBALANCE\t|\tTYPE");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                counter += 1;
            }
            Console.Write("\n\n\nWrite vehicle sequence number: ");
            var vehicleIndex = Int32.Parse(Console.ReadLine());
            var vehicleId = _parkingService.GetVehicles()[vehicleIndex - 1].Id;
            Console.Write("Write top up amount: ");
            var sum = Int32.Parse(Console.ReadLine());
            _parkingService.TopUpVehicle(vehicleId, sum);
            Console.WriteLine("Topped up successfully!");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        protected class Option
        {
            public string Name { get; }
            public Action Selected { get; }

            public Option(string name, Action selected)
            {
                Name = name;
                Selected = selected;
            }
        }
    }
}
