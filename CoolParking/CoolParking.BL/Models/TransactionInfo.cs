﻿
using Newtonsoft.Json;
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string ActionMessage { get; }
        public decimal Sum { get; }
        public DateTime DateTime { get; }
        public string VehicleId { get; }

        public TransactionInfo(string vehicleId, decimal sum)
        {
            VehicleId = vehicleId;
            Sum = sum;
            DateTime = DateTime.Now;
            ActionMessage = $"{DateTime}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.\n";
        }

    }
}