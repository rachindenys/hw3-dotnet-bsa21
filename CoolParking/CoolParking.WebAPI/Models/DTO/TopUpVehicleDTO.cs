﻿namespace CoolParking.WebAPI.Models.DTO
{
    public class TopUpVehicleDTO
    {
        public string Id { get; set; }
        public decimal? balance { get; set; }
    }
}
