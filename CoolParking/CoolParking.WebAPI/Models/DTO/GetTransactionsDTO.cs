﻿using System;

namespace CoolParking.WebAPI.Models.DTO
{
    public class GetTransactionsDTO
    {
        public decimal Sum { get; set; }
        public DateTime TransactionDate { get; set; }
        public string VehicleId { get; set; }
    }
}
