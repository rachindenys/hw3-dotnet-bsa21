﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models.DTO
{
    public class VehicleDTO
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; }
        [JsonProperty("balance")]
        public decimal Balance { get; internal set; }
    }
}
