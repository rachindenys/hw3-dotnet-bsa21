﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ASP.NET_Core_Lecture.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;
        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET api/vehicles
        [HttpGet]
        public IReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parkingService.GetVehicles();
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            if (!Regex.IsMatch(id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                return BadRequest("Bad ID format!");
            }
            var vehicle = _parkingService.GetVehicles(id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found!");
            }
            return _parkingService.GetVehicles(id);
        }

        //POST api/vehicles

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody] Vehicle vehicle)
        {
            //VehicleDTO vehicleDTO = (VehicleDTO)JsonConvert.DeserializeObject(content);
            if (!(Regex.IsMatch(vehicle.Id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
                || (int)vehicle.VehicleType > 4
                || (int)vehicle.VehicleType < 1
                || vehicle.Balance < 0)
            {
                return BadRequest("Wrong request body!");
            }
            _parkingService.AddVehicle(vehicle);
            HttpContext.Response.StatusCode = 201;
            return vehicle;
        }
        // { "id": "LJ-4812-GL", "vehicleType": 2, "balance": 100 }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            if (!Regex.IsMatch(id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                return BadRequest("Bad ID format!");
            }
            var vehicle = _parkingService.GetVehicles(id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found!");
            }
            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}
