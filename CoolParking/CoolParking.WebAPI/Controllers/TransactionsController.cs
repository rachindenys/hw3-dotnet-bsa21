﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ASP.NET_Core_Lecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;
        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET api/transactions/last
        [HttpGet("last")]

        public ActionResult<IEnumerable<GetTransactionsDTO>> GetLastTransactions()
        {
            var lastTransactions = _parkingService.GetLastParkingTransactions();
            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<TransactionInfo, GetTransactionsDTO>()
                .ForMember(dest => dest.TransactionDate, act => act.MapFrom(src => src.DateTime))
                );
            var mapper = new Mapper(config);
            List<GetTransactionsDTO> lastTransactionsDTO = new List<GetTransactionsDTO>();
            return Ok(mapper.Map<IEnumerable<GetTransactionsDTO>>(lastTransactions));
        }

        // GET api/transactions/all (только транзакции с лог файла)
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            return Ok(_parkingService.ReadFromLog());
        }

        // PUT api/transactions/topUpVehicle

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUpVehicleDTO content)
        {
            if (!Regex.IsMatch(content.Id, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$"))
            {
                return BadRequest("Bad ID format!");
            }
            var vehicle = _parkingService.GetVehicles(content.Id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found!");
            }
            if (content.balance == null )
            {
                return BadRequest("Bad body request!");
            }
            _parkingService.TopUpVehicle(content.Id, (decimal)content.balance);
            return Ok(vehicle);
        }
    }
}