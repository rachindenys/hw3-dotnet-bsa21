﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ASP.NET_Core_Lecture.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;
        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET api/parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [HttpGet("income")]
        public ActionResult<decimal> GetCurrentIncome()
        {
            return Ok(_parkingService.GetCurrentIncome());
        }

        // GET api/parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        // GET api/parking/freePlaces

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}